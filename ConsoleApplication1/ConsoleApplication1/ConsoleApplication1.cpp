// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <iomanip> 
#include <iostream>
#include <algorithm>
#include <windows.h>
using namespace std;

int boardSize;
const int NROFMOVES = 8;
int turnNr = 0;

const int MOVES[8][2] = {
	{ -1, -2 } ,
	{ -2, -1 } ,
	{ -2, 1 } ,
	{ -1, 2 } ,
	{ 1, 2 } ,
	{ 2, 1 } ,
	{ 2, -1 } ,
	{ 1, -2 } ,
};

void ResetBoard(int ** board, int * boardSize) 
{
	turnNr = 0;

	// Setup the board.
	for (int i = 0; i < *boardSize; i++)
		for (int j = 0; j < *boardSize; j++)
			board[i][j] = -1;
}

void PrintBoard(int ** board, const int * boardSize) 
{
	cout << "\n Board: \n";
	cout << "----------------------------------------\n";
	for (int i = 0; i < *boardSize; i++)
	{
		for (int j = 0; j < *boardSize; j++)
		{
			if (board[i][j] != -1) 
			{
				cout << "|";
				cout << setw(3) << board[i][j];
				cout << "|";
			}
			else
				cout << setw(5) << "|   |";
		}
		cout << "\n";
		cout << "----------------------------------------\n";
	}
	cout << "\n";
}

bool CheckMove(int ** board, const int * row, const int * col, const int * boardSize)
{
	if (*row >= 0 && *row < *boardSize
		&& *col >= 0 && *col < *boardSize
		&& board[*row][*col] == -1)
		return true;
	else
		return false;
}

int FindBestMove(int **board, const int * startRow, const int * startCol, const int * boardSize, const int * turn)
{
	// Check all move options for amount of possible moves.
	int moveOpts[NROFMOVES];
	std::fill_n(moveOpts, NROFMOVES, 0);
	int curMoveRow, curMoveCol;
	int curOptRow, curOptCol;
	for (int i = 0; i < NROFMOVES; i++)
	{
		curMoveRow = *startRow + MOVES[i][0];
		curMoveCol = *startCol + MOVES[i][1];
		if (CheckMove(board, &curMoveRow, &curMoveCol, boardSize)) 
		{
			if (*turn == *boardSize**boardSize - 1)
				return i;

			// Set to marked temporarily.
			board[curMoveRow][curMoveCol] = turnNr;

			for (int j = 0; j < NROFMOVES; j++)
			{
				curOptRow = curMoveRow + MOVES[j][0];
				curOptCol = curMoveCol + MOVES[j][1];
				if (CheckMove(board, &curOptRow, &curOptCol, boardSize))
				{
					moveOpts[i]++;
				}
			}

			// Unmark.
			board[curMoveRow][curMoveCol] = -1;
		}
	}

	// DEBUG
	//for (int i = 0; i < NROFMOVES; i++)
	//{
	//	cout << "\n Move option for index " << i << " is: " << moveOpts[i];
	//}

	// Check for the option with the lowest possible moves.
	int bestMoveCount = 0;
	int lwstMove = 9;
	int lwstMoveI = -1;
	int moveIndexes[8];
	std::fill_n(moveIndexes, 8, -1);
	for (int i = 0; i < NROFMOVES; i++) 
	{
		if (moveOpts[i] != 0
			&& moveOpts[i] <= lwstMove)
		{
			bestMoveCount += 1;
			lwstMove = moveOpts[i];
			lwstMoveI = i;
			moveIndexes[i] = i;
		}
	}

	// DEBUG
	//for (int i = 0; i < NROFMOVES; i++)
	//{
	//	cout << "\n Move index for index " << i << " is: " << moveIndexes[i];
	//}

	// If there is only a single choice, return it.
	if (bestMoveCount == 1) 
		return lwstMoveI;
	// Multiple choices, return a random choice...
	else if (bestMoveCount != 0) 
	{
		// Fill the array with indexes to the lowest moves.
		int fillCount = 0;
		int *bestMoves = new int[bestMoveCount];
		for (int i = 0; i < NROFMOVES; i++)
		{
			if (moveIndexes[i] >= 0) {
				bestMoves[fillCount] = moveIndexes[i];
				fillCount++;
			}
		}

		// Random choice.
		random_shuffle(&bestMoves[0], &bestMoves[bestMoveCount]);
		int choice = bestMoves[0];

		// Delete the dynamically allocated array.
		delete[] bestMoves;

		return choice;
	}
	// No choice left.
	else
		return -1;
}

bool Solve(int **board, const int * startRow, const int * startCol, const int * boardSize)
{
	board[*startRow][*startCol] = 0;
	int curRow = *startRow, curCol = *startCol;
	// Run for the amount of positions on the board.
	for (int i = 1; i < *boardSize * *boardSize; i++)
	{
		//Sleep(250);
		//PrintBoard(board, boardSize);
		int bestMove = FindBestMove(board, &curRow, &curCol, boardSize, &i);
		if (bestMove != -1) 
		{
			// DEBUG
			//cout << "\n Best move: " << bestMove;
			// Set the new position.
			curRow += MOVES[bestMove][0];
			curCol += MOVES[bestMove][1];
			// Mark the new position as visited.
			board[curRow][curCol] = i;
		}
		else 
			// No more possible moves, no solution.
			return false;
	}
	// Found a solution.
	return true;
}

int main()
{
	cout << "Please enter a size for the board (N x N) (bigger than 5x5): ";
	cin >> boardSize;

	// Initiliaze the board.
	int** board = new int*[boardSize];
	for (int i = 0; i < boardSize; i++)
		board[i] = new int[boardSize];
	
	// Setup board.
	ResetBoard(board, &boardSize);
	
	int startRow, startCol;
	cout << "Please enter a start row (0, " << boardSize - 1 << "): ";
	cin >> startRow;
	cout << "Please enter a start collum (0, " << boardSize - 1 << "): ";
	cin >> startCol;

	while (!Solve(board, &startRow, &startCol, &boardSize))
	{
		//cout << "\nDid not find a solution this time, trying again! Try: " << count;
		ResetBoard(board, &boardSize);
	}
	cout << "\n\n\nFound a solution!";
	PrintBoard(board, &boardSize);

	cout << "\n\n\n";
	system("pause");
    return 0;
}

